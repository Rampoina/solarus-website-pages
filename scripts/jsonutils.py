import json
import os
import glob

# Simple separator replacement for path values in JSON
def get_unix_path(path):
  return path.replace("\\", "/")

# Get the relative path of file_b from file_a
def get_relative_path(file_a, file_b):
  path_a = os.path.abspath(file_a)
  path_b = os.path.abspath(file_b)
  return os.path.relpath(path_b, path_a)

# Write the dictionary into a JSON file
def write_json(file_path, data):
  data_as_txt = json.dumps(data, indent=2, sort_keys=True, ensure_ascii=False)
  with open(file_path, "w") as f:
    f.write(data_as_txt)
    f.close()
    return True
  return False

# Finds the property in the JSON hierarchy
def find_property_in_file(file_path, property_name, add_missing=False, callback=None):
  # Open the file
  with open(file_path, "r") as f:
    data = json.load(f)
    # Reference to working data
    current_data = data

    # Get property hierarchy
    hierarchy = property_name.split('/')
    
    # Walk the hierarchy in the JSON.
    hierarchy_last = len(hierarchy) - 1
    for idx, hierarchy_level in enumerate(hierarchy):
      if hierarchy_level in current_data:
        # We've found the right level in the hierarchy.
        if idx == hierarchy_last:
          if not callback is None:
            modified = callback(current_data, hierarchy_level)
            if modified:
              write_json(file_path, data)
            return modified
          else:
            return False
        else:
          # Keep on iterating onto the sub-level.
          current_data = current_data[hierarchy_level]
      else:
        # If we DON'T find the hierarchy level in data,
        # we add the level in the hierarchy.
        if add_missing:
          if idx == hierarchy_last:
            if not (callback is None):
              modified = callback(current_data, hierarchy_level)
              if modified:
                write_json(file_path, data)
              return modified
            else:
              return False
          else:        
            # Add, then keep on iterating onto the sub-level.
            current_data[hierarchy_level] = {}
            current_data = current_data[hierarchy_level]
        else:
          break

  return False

# Add property to the page if not present
def add_property_to_file(file_path, property_name, property_value, property_is_file=False):
  def add_keyvalue(dic, key):
    if property_is_file:  
      dic[key] = get_unix_path(get_relative_path(os.path.dirname(os.path.dirname(os.path.realpath(file_path))), os.path.realpath(property_value)))
    else:
      dic[key] = property_value
    return True

  return find_property_in_file(file_path, property_name, add_missing=True, callback=add_keyvalue)

# Remove a property to the page if present
def remove_property_to_file(file_path, property_name):
  def remove_item(dic, key):
    del dic[key]
    return True

  return find_property_in_file(file_path, property_name, add_missing=False, callback=remove_item)

# Walk all JSON files
def process_all_files(path, property_name, callback, recursive=True, verbosity=True):
  # System-related files that we should not touch
  excluded_files = { "structure.json", "config.json", "menus.json", "redirects.json"}

  if verbosity:
    print("\nProcessing files...\n")
  total_files = 0
  modified_files = 0
  for filename in glob.iglob(path + "/**/*.json", recursive=recursive):
    if not os.path.basename(filename) in excluded_files:
      total_files += 1
      if verbosity:
        print("Processing: " + get_unix_path(filename))      
      modified = callback(filename, property_name)
      if modified:
        if verbosity:
          print("  Modified!")
        modified_files += 1
  print("\nDone!")
  print("Total files:    " + str(total_files))
  print("Modified files: " + str(modified_files))
