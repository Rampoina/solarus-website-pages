# {title}

[youtube id="M76i06qEX6A"]

## Summary

- Working with dynamic tiles
  - Making a ladder appear when a switch is activated
  - Draining water when a switch is activated

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
