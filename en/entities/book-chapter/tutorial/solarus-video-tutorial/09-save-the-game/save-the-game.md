# {title}

[youtube id="fPdFOG24dE4"]

## Summary

- Pausing the game
- Creating a save game dialog
- Working with question dialogs
- Saving the game
- Structure of the savegame file
- Setting savegame values

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
