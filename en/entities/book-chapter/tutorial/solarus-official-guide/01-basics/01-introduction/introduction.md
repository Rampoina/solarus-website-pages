# {title}

## What is Solarus

![Engine logo](engine-logo.png)

Solarus is a **free and open-source game engine**, licensed under GPL v3. It is written from scratch in C++ and uses SDL2. Consequently, it is compatible with a large set of platforms. You can [explore the source code](https://gitlab.com/solarus-games) if you're curious.

The goal of the engine is to allow people to create their own Action-RPG games (also called Zelda-like games). It is totally independent from Nintendo, and will be free forever.

The engine itself does not contain any copyrighted code or assets: it is 100% free and open-source. Thus, Solarus cannot be eligible for a DMCA from anyone, including Nintendo.

Solarus currently only support **2D graphics**. However, your game does not have to look like a 16-bit game: artstyle is totally up to the developer, and OpenGL shaders can be used to get a modern feel.

## What is *not* Solarus

![Solarus is not a SNES emulator](snes-crossed.png)

Solarus is not an Super Nintendo™ emulator, neither a *The Legend of Zelda: A Link To the Past*™ ROM hack.

**Solarus is not a "Zelda engine"** as it can be read sometimes; it can power any Action-RPG or Action-Adventure game (or any kind of game you want, with a little more work). *A Link To The Past* assets (tilesets, sprites, musics, etc.) are totally separated from the engine (the engine does not contain them).

However, it would be lying to deny *Zelda* had a great influence on Solarus, because historically it was first designed as an engine to power a *Zelda* game.

Also, **Solarus does not currently allow you to create a game without coding**, unlike RPG Maker. In practice, it means you can create maps and sprites with the mouse, but you'll have to code scripts in Lua to manage the game logic and events: trigger a door opening, or give the hero a new weapon for instance. Fortunately, Lua is a simple language and should not frighten people who've never coded. It's a great way to start and learn.

## What has Solarus to offer

Solarus is a very lightweight engine, and is very easy to develop with. It provides tools for game developers and players.

* **Solarus Quest Editor** is an IDE that allows the game developers to edit their quests, i.e. Solarus games: graphically modifying maps, sprites, etc. It even has a basic Lua code editor.
* **Solarus Launcher** is an app that non-experienced players can use to launch Solarus games, a bit like an emulator, if they don't want to use a terminal. It allows Solarus game developers to package and deliver their quests easily.

Solarus offers a **Lua API** so all your game's code can be done in Lua. The C++ engine will take car of all heavy computation, and you'll just have to write the game's logic. In theory, you shouldn't have to dig into the C++ code.

Solarus is very-much orientated towards **Action-RPG or Action-Adventure games**. The Lua API already contains lots of tools used to build this kind of games: maps, items, enemies, treasures, etc. Using Solarus for that kind of game will greatly reduce your development time.
