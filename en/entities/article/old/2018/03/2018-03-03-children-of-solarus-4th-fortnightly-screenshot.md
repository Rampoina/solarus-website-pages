Ahoy there again! Today we have our worst screenshot until now of the project Children of Solarus. But do not worry, because the next ones may have tasty surprises! :D

Our screenshot shows Eldran jumping with the new custom jump, which is much better than the built-in jump used in MoS-DX. Some annoying pink flowers and a mysterious wooden house can be seen.

Challenge: Can you guess what is written in the signpost in morse code? :)

<a href="http://www.solarus-games.org/wp-content/uploads/2018/03/4.-18-03-04.png"><img class=" wp-image-1708 aligncenter" src="/images/4.-18-03-04-300x225.png" alt="" width="537" height="403" /></a>

We are coming back in 2 weeks with more news. Stay tuned!