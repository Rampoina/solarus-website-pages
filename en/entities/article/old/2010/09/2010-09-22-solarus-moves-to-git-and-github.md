I have just switched the source code from Subversion to <a href="http://github.com">Git</a>. The code can now be accessed on <a href="http://github.com/christopho/solarus">github</a>, which provides a nicer interface for the history of commits and many more  features.

Your svn local copy should be replaced by a local fork of the git repository. Everything is explained on the <a href="http://github.com/christopho/solarus">Solarus github</a> page, but to be short, use the following command to download the code (obviously, git needs to be installed):
<pre>git clone git://github.com/christopho/solarus.git</pre>
You now have a local fork of the repository, including the full history of commits. You may modify the code and even do your own local commits since git is a distributed versioning system. Then, if you want me to include your modifications into the official repository, you can make a request with git. Feel free to contribute!

Note: the SVN repository is still available but is now obsolete. New commits are only made on the Git repository. The SVN repository remains at revision 1443.