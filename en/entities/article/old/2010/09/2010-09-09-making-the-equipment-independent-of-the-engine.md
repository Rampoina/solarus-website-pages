Up to now, the equipment is hardcoded in the C++ engine. If you are developping your own quest with the Solarus engine, you cannot add new items or remove the ones you don't want, unless you modify the C++ code. By "items", I refer here to everything that the player can find: a sword, a shield, a boomerang, some bombs, some rupees, a piece of heart, a bottle, a key, etc. The list of items is defined in the C++ code, as well as their behavior.

I am currently working on making items independent of the engine. This is an important step for making the engine not specific to a particular game. The ultimate objective is to have a configuration file that describes each item of your quest. Such a file might have an ini-like syntax where each item is described in a group.

The hookshot would be described like this:
<pre>[item.hookshot]
name = Hookshot
savegame_variable = 1105
can_be_assigned = true</pre>
Here we have declared an item with the id "hookshot" and the human-readable name "Hookshot", useful for the editor. Its possession state (0 or 1) is saved in the variable #1105. It can be assigned, i.e. the player can associate it to an item key and use it explicitely by pressing this key.

A Lua script gives the behavior of the item. It defines what happens when the player uses it explicitely (for items that can be assigned to an item key). It can also define what ability the simple fact to have the item gives (for example, the flippers give the ability to swim and the player does not need to assign them to an item key).

Some items have several variants. For such items, the possession state may have a value greater than one. For example, we can imagine that the sword has four variants:
<pre>[item.sword]
name = Sword
nb_variants = 4
ability = sword</pre>
Some items have a counter associated to them:
<pre>[item.bombs]
name = Bombs
savegame_variable = 1101
can_be_assigned = true
counter = 1024</pre>
Here, the savegame variable #1024 stores the current number of bombs of the player (whereas the variable #1101 indicates whether he has the bombs). The number of bombs may be limited by a fixed number, or even by another item:
<pre>[item.bomb_bag]
name = Bomb bag
savegame_variable = 1032
limit_for_counter = bombs
nb_variants = 3
amount_1 = 10
amount_2 = 30
amount_3 = 99</pre>
A third item can update the number of bombs (note that this one is not saved):
<pre>[item.bombs_refill]
name = Bombs refill
changes_counter = bombs
nb_variants = 3
amount_1 = 1
amount_2 = 5
amount_3 = 10
brandish_when_picked = false
can_disappear = true</pre>
I agree with you: describing all items of your quest like this would be great. This is what I am working on. The treasures, the pickable items, the inventory items and the dungeon items will depend on this file. None of them will be hardcoded anymore. It requires a lot of work in many parts of the engine (not only the items) : the savegames, the Lua API, the HUD, the pause subscreens, etc. The format of maps is also changing because some entities rely on the item system: the chests, the shop items, the pickable items and the entities that hold pickable items (enemies and destructible items).

When this work is finished, your existing maps, scripts and savegames will not be compatible anymore (of course, I will provide some documentation to explain how to upgrade them).