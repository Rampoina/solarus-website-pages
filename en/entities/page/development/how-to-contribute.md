[container]

[row]
[column width="8"]

# {title}

So you want to contribute to Solarus? All the community is thankful and any help, even very small, is welcome. Come talk with us on [Discord](https://discord.gg/yYHjJHt) to inform us about what you think you can bring to the project.

## Development

All of the Solarus projects source codes are on Gitlab. The engine is made in C++. It is recommended to be proficient in this language and to master video game programming patterns before being able to contribute. The Quest Editor and the Launcher are made in C++ with Qt.

* [Solarus projects on Gitlab](https://gitlab.com/solarus-games)
* [C++ Engine Code documention](http://www.solarus-games.org/developer_doc/latest)

If you want to **inform of a bug**, or if you have a **feature request**:

1. Look if anyone hasn't already created an issue in the issue tracker of the project.
2. Create your issue, and be as precise as possible. Join screenshots if needed.

If you want to **add or modify the source code** of any project:

1. Clone the repository you want to contribute to.
2. Create your own branch and make your modifications in this branch.
3. Create a Merge Request.

## Tutorials for quest makers

So you know quite well how to make a Solarus quest? Why not help people to learn it too? Tutorials need to be written to help begineers and more advanced quest makers. We have a repository on Gitlab for the official tutorials. They are written in markdown, which is pretty easy to learn.

* [Solarus tutorials on Gitlab](https://gitlab.com/solarus-games/learn-solarus)

Follow the same steps than developers of you want to contribute to this project:

1. Clone the repository.
2. Create your own branch and make your modifications in this branch.
3. Create a Merge Request.

## Website

### Website improvements

The Solarus website uses [Kokori](https://gitlab.com/solarus-games/kokori/), our own free and open-source PHP backend, using Markdown files for content. We are far from finished with this website and help is very much welcomed.

You may contribute by 2 ways:

1. Improve the Kokori website engine
2. Add content to the website

To understand how Kokori works, we made a tutorial as well ass documentation. Please keep in my that it is a work in progress.

### Adding a game to the Solarus Quest Library

If your game development is complete, or enough advanced, you can add it to the Games pages of this website, which makes an inventory of games made with Solarus.

For this, we wrote a tutorial. Create a Merge Request when your branch is ready to be integrated, and we'll check if your game is eligible as well as the added files' correctness.

* [How to add a game to the Solarus Quest Library](https://gitlab.com/solarus-games/solarus-website-pages/blob/dev/guidelines/game.md)

## Art

If you're feeling artistic, we need help in the pixel art department. Any tileset, font or sprite contributions are welcome! You may also want to improve the website design?

* [Solarus graphic design elements on Gitlab](https://gitlab.com/solarus-games/solarus-design)
* [Solarus free resource pack on Gitlab](https://gitlab.com/solarus-games/solarus-free-resource-pack)

## Translations

### Game Editor and Game Launcher

The Quest Editor and the Launcher need to be translated in all the existing languages! Feel free to add your own language. Since they're built with Qt, you should Qt built-in translation manager named Qt Linguist. Qt offers [documentation about translating a Qt app](https://doc.qt.io/qt-5/qtlinguist-index.html).

### Games

The games may have new translations too. We have an in-house translator tool integrated into Solarus Quest Editor. Please read the [documentation about translating a quest](https://www.solarus-games.org/doc/latest/translation.html).

### Website

This website may also be translated in your language. Be aware that it may be demanding because you must ensure the translation follows closely regular updates on the website (pages, news, tutorials). We always maintain French and English, because we can speak these languages. However, we won't maintain other languages we can't speak. Please read [documentation about translating the website](https://gitlab.com/solarus-games/solarus-website-pages/blob/dev/guidelines/translation.md).

## Donations

Solarus developers create this engine on their spare time. If you appreciate their work, your donations can help with the hosting fees and show that Solarus is appreciated.

* [Donation page](donation)

[/column]

[column width ="4"]
[summary level-min="2" level-max="3"]
[/column]

[/row]
[/container]