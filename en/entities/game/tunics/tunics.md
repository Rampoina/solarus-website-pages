### Presentation

*Tunics!* is a Rogue-like *Legend of Zelda* quest. It means it's only dungeons, and they are randomly generated. Moreover, once you're dead, you'll have to restart from the beginning, losing all your improvements (weapons, health, etc). It should be considered a well-polished proof-of-concept.
