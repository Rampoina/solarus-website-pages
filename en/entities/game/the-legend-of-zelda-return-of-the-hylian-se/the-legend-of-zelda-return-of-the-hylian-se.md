### Presentation

*The Legend of Zelda: Return of the Hylian SE* is a Solarus-made remake of Vincent Jouillat's first game, hence the *SE* suffix (stands for *Solarus Edition*). It was first released on 12th August, 2006, and made with another custom engine in C++. It has been remade entirely in Solarus to take advantage of the better capabilities of the engine.

### Synopsis

After Link's victory over Ganon in *A Link to the Past*, no one knows what Link’s wish to the Triforce was. But this wish reunified the Light World and the Dark World and brought the Seven Wise Men’s descendants back to life. Peace was back in Hyrule. But unfortunately, this wish also ressurected Ganon and his henchmen. He was preparing his revenge, but he couldn’t do anything without the Triforce.

One night, a familiar voice speaks to Link in his sleep...

![Link](artworks/artwork_link.png "Link")
