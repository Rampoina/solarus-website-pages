### Presentation

*The Legend of Zelda: Onilink Begins SE* is a Solarus-made remake of Vincent Jouillat's second game, hence the *SE* suffix (stands for *Solarus Edition*). It was first released on 12th August, 2007, and made with another custom engine in C++. It has been remade entirely in Solarus to take advantage of the better capabilities of the engine.

### Synopsis

Brought down by a terrible curse since his recent victory on the Dark Lord, Link is changing, day by day, into a powerful creature with a destructive nature named Oni-Link. Banished from Hyrule, the young hylian asks the Princess Zelda some help. She shows him his last hope: a portal to a secret world.

![Link](artworks/artwork_link.png "Link")
