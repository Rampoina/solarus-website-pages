### Presentation

*The Legend of Zelda: Mystery of Solarus DX* is set to be a direct sequel to *The Legend of Zelda: A Link to the Past* on the SNES, using the same graphics and game mechanisms. *Mystery of Solarus DX* is the first game made with the Solarus engine and in fact, Solarus was primarily designed for this game.

*Mystery of Solarus DX* is actually an enhanced remake of a first creation, *Mystery of Solarus* (without the *DX* suffix). This first creation, developed with RPG Maker 2000, was released in 2002 and was only available in French. The *DX* project was unveiled on April 1st, 2008. Its objectives were to correct the many flaws of its predecessor: the battle system, the bosses, the use of items, etc.

However, that is hardly all of it, as new graphical elements and musics have been created to accompany you throughout the game. This Deluxe version is the opportunity for you to relive the original adventure in a brand new way, or to discover it for the first time if you’ve never played it before!

![Link](artworks/artwork_link.png "Link")

### Synopsis

In *A Link to the Past*, Ganon was defeated and forever imprisonned in the Golden Land by Hyrule's King, thanks to the Seal of the Seven Sages.

![The infamous Ganon](artworks/artwork_ganon.png "The infamous Ganon")

Years passed, and someday the King became affected by a strange and grave illness. Doctors and apothecaries from across the kingdom tried in vain to cure him. The king passed away, and his power vanished, weakening the Seal of the Seven Sages.

![Sahasrahla the elder](artworks/artwork_sahasrahla.png "Sahasrahla the elder")

The Hero, under the guidance of his master Sahasrahla, trusted the heiress of the royal powers, Princess Zelda, with the Triforce. Zelda teamed with eight mysterious children to shatter the Triforce into eight fragments and conceal them across the kingdom. Peace was restored.

![Princess Zelda](artworks/artwork_zelda.png "Princess Zelda")

And so begins your journey, full of monsters, mysterious characters, labyrinths and puzzles...

![Billy](artworks/artwork_billy.png "Billy")
