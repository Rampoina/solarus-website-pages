### Synopsis

The game takes place just after the events of the first chapter, which was a little game with just one short dungeon beatable in 40 min, from 2017.

After having touched the Trophy of Victory won at the end of the first chapter's dungeon, Link is teleported in High-rule, a parallel world where multiple video games universes are mixed up.
Link must go to the Tower of Memories where Zeldo is waiting for him to get his revenge!

### Gaming system

The action of the game is located in Takapa's County, at the west of High-rule. Link is free to go directly to the Tower of Memories to try Zeldo's Challenge or explore this brand new world. The game is in the same vein than ALTTP but with in wackier universe than the typical *Legend of Zelda* games, to which multiple video games characters join, and lots of references.

To complete the 100% though, it will be mandatory to explore the outside of the tower, since the tower contains items Link cannot totally explore Takapa's County without.

### Playtime

The game is rather short. You may beat it in 1 hour if you try to rush the main quest, but getting  the 100% might take 3 to 6 hours, depending on your skills and exploration.

At the end of the game, a statistics screen will show you your completion and your percentage will be computed, as in Vincent Jouillat's *Zelda* games. An achievement system, also taken from these games, has been implemented, for the most completionists players' pleasure!

### Main quest and side quests

The game has one main dungeon: the Tower of Memories, which is the main quest of the game. However, multiple side-quests and mini-dungeons are to be discovered, using familiar mechanisms from *Zelda* games: trading quest, pieces of heart... The most dedicated players might even get into the Power Moons research, a mysterious quest that will really put to the test their exploratory skills.
