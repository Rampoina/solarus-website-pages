### Presentation

*The Legend of Zelda*, publié en 1986 sur Nintendo Entertainment System, est le premier épisode de l'immensément influente saga qui conduira finalement à la création de Solarus.

Les ressources de ce pack sont rassemblées progressivement [sur le forum anglophone](http://forum.solarus-games.org/index.php/topic,1365.0.html), et espérons qu'un jour il soit possible de recréer ce jeu vidéo fondateur avec Solarus.

![The Legend of Zelda NES box](images/box_art.png)
