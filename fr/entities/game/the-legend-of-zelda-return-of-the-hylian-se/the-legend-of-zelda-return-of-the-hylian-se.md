### Présentation

*The Legend of Zelda: Return of the Hylian SE* est un remake fait avec Solarus du premier jeu de Vincent Jouillat, d'où le suffixe *SE* qui signifie *Solarus Edition*. Le jeu d'origine avait été initialement publié le 1er Août 2006, mais a été refait entièrement avec Solarus pour profiter des meilleurs capacités du moteur.

### Synopsis

Après la victoire de Link sur Ganon dans *A Link to the Past*, personne ne sut quel fut le souhait que Link fit à la Triforce. Mais ce souhait réunifia le Monde de la Lumière et le Monde des Ténéèbres, et ressuscita les descendants des Sept Sages. La paix était de retour à Hyrule. Malheureusement, ce souhait ressuscita aussi Ganon et ses sbires. Ce dernier préparait sa revanche, mais ne pouvait rien faire sans le pouvoir de la Triforce.

Une nuit, une voix familière parle à Link pendant son sommeil…

![Link](artworks/artwork_link.png "Link")
