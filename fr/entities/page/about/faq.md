[container]

[row]
[column width="8"]

# {title}

## À propos de Solarus

### Qu'est-ce que Solarus ?

Solarus est un **moteur de jeu libre et open-source**, sous licence GPL v3. Il est **écrit en C++** et utilise SDL2. Par conséquent, il fonctionne sur un grand nombre de systèmes. Ce n'est ni un émulateur, ni un ROM hack de Super Nintendo™.

Le dévelopmment a commencé en 2006. Le but du moteur est de **permettre aux gens de créer leur propre jeu Action-RPG** (genre aussi appelé Zelda-like). Il est totalement indépendant de Nintendo.

**Solarus ne contient aucun code ou données propriétaire**: il est 100% libre et open-source. Ainsi, Solarus ne peut pas être accusé de violation de droit d'auteur par quiconque, dont Nintendo.

Solarus n'est pas un "moteur de jeu Zelda" comme on peut parfois le lire; il peut être utilisé pour n'importe quel jeu Action-RPG ou Action-Aventure (et même n'importe quel type, moyennant un peu plus de travail). Les données issues du jeu *A Link to the Past* sont totalement séparées du moteur (elles ne sont pas inclues dans celui-ci).

### Qu'est-ce que Solarus Quest Editor ?

Solarus Quest Editor est un **éditeur de jeu** libre et open-source pour Solarus. Écrit en **C++ avec Qt**, il est sous licence GPL v3. Le but de cet éditeur est de faciliter la création de jeux avec Solarus. Le logiciel dispose d'éditeurs pour les maps, les tilesets, les sprites, les scripts Lua, parmi d'autres fonctionnalités.

### Qu'est ce qu'une quête Solarus ?

Une quête Solarus est tout simplement un jeu fait avec Solarus.

### Pourquoi y a t-il plusieurs fichiers exécutables dans mon téléchargement ?

Ces fichiers sont :

* **Solarus Launcher:** l'interface graphique pour gérer votre ludothèque Solarus et lancer les quêtes.
* **solarus_run:** le programme en ligne de commande pour lancer une quête. C'est totalement normal si vous l'exécutez sans lui donner en paramètre la quête à jouer.
* **Solarus Quest Editor:** l'interface graphique pour éditer des quêtes Solarus.

### Qu'est-il possible de faire avec Solarus ?

Nous entendons souvent la question "Est-ce que je peux faire ceci avec Solarus ?". La réponse sera probablement oui.

Solarus possède une API Lua, et vous pouvez programmer ce que vous voulez en Lua. Par conséquence, réussir à faire quelque chose avec Solarus est presque toujours possible, si vous êtes prêt à y investir le temps nécessaire. Cependant, le moteur ne supporte pas les graphismes 3D : c'est l'unique limitation à votre imagination. N'importe quel genre de jeu est possible.

## À propos des licences

### Êtes vous affilié à Nintendo ?

**Non, nous sommes indépendant de Nintendo.** Le moteur de jeu Solarus n'a aucun contenu propriétaire et est écrit comme un moteur de jeu Action-RPG généraliste, c-a-d pas seulement de jeux *The Legend of Zelda*.

However, **certains jeux faits avec Solarus utilisent des assets propriétaires de Nintendo**, principalement de *The Legend of Zelda: A Link To The Past*. C'est pour des raisons historiques et nous faisons notre possible pour les remplacer par des assets Creative Commons. Pour être clair, Solarus a commencé comme un simple moteur pour le jeu *Zelda* amateur *Mystery Of Solarus DX*. Nous estimons que cet usage est raisonnable.

### Puis-je utiliser Solarus pour un jeu commerical ?

Premièrement, sachez que votre jeu est divisé en 2 parties :

* **Le moteur:** Solarus (que vous pouvez modifier ou utiliser tel quel).
* **La quête:** les données du jeu (maps, scripts, sprites, dialogs, etc.).

**Le moteur est GPL**, donc certaines règles doivent être respectées :

* Si vous utilisez Solarus tel quel : fournissez s'il-vous-plait un lien vers le code source de Solarus.
* Si vous modifier Solarus et publiez votre jeu, vous devez fournir un accès au code source de votre version modifiée de Solarus. De plus, cette version modifiée sera elle aussi automatiquement sous licence GPL.

**Les données de votre jeu peuvent avoir la licence que vous souhaitez**. Cela peut être différent de GPL et rester du code et du contenu privé. Les licences commerciales sont autorisées pour votre quête.

* Les données et les scripts faits depuis zéro par vos soins peuvent être sous la licence que vous voulez.
* Les données et les scripts réutilisés ou modifiés doivent suivre les règles de leur licence d'origine. Faites attention aux scripts GPL ou données CC : ils doivent conserver la licence d'origine lorsqu'ils sont modifiés. Notez aussi qu'utiliser du contenu propriétaire dont vous n'avez pas les droits est interdit par la loi.

### Est-ce que je peux utiliser les sprites, personnages et musiques de Zelda ?

**Pour un jeu commercial, évidemment: NON.** N'utilisez jamais de contenu copyrightés par quiconque, Nintendo inclus (sprites, personnages, musique, histoire...). Ou vous vous exposez à de sérieux problèmes légaux. Vous ne pouvez pas dire qu'on ne vous avait pas prévenu(e).

**Pour un jeu non-commercial**, oui, mais restez petits. C'est ce que nous faisons. À moins que Nintendo ne vous dise d'arrêter, cela semble possible. Nintendo a l'air de tolérer les créations de fans à but non-lucratif, tant qu'elles ne deviennent pas trop populaires et n'empiettent pas sur les ventes de leurs propres jeux, ce qui est totalement compréhensible et logique.

Nous estimons qu'utiliser du contenu *Zelda* comme base pour apprendre le développement de jeux est **raisonnable**. Cependant, vous devriez remplacer progressivement tout ce contenu propriétaire par du contenu personnalisé ou libre, dans le but de ne pas violer la loi.

## À propos des jeux

### J'ai téléchargé un jeu et ça ne fonctionne pas

Il y a plusieurs possibilités, dont voici les plus courantes :

#### Vous confondez le moteur et la quête

Vous avez peut-être confondu le moteur et la quête, qui sont deux logiciels différents.

Pour lancer une quête, vous avez plusieurs possibilités :

* **Depuis le terminal:** Lancez solarus (c-a-d le moteur) et spécifiez le chemin de la quête en paramètre. Vous pouvez aussi placer le dossier data de la quête dans le même dossier que le moteur et lancer ce dernier : la quête sera automatiquement détectée et utilisée par le moteur. Écrivez `solarus_run chemin/vers/nom-de-quete` dans le terminal.

* Vous pouvez aussi utiliser **Solarus Launcher** qui est une interface graphique (pour les réfractaires au terminal). Importez votre jeu dans la bilbiothèque, sélectionnez-le et cliquez sur Jouer.

#### Votre version de Solarus n'est pas à jour

Vérifiez que la **version de Solarus requise pour votre quête** est compatible avec la **version de Solarus**. Si non, mettez Solarus à jour.

Notez que le moteur est retro-compatible. Cela signifie que vous pouvez jouer à une quête faite pour une ancienne version de Solarus avec un version plus récente du moteur.

### Est-ce que les jeux fonctionnent sur SNES (Super Nintendo) ?

**Non, ils ne fonctionnent pas sur SNES**, même si *Mystery of Solarus DX* ressemble beaucoup à *The Legend Of Zelda : A Link To The Past*. Solarus n'a pas été écrit pour fonctionner sur Super Nintendo, et personne n'a encore porté le moteur sur cette console. Solarus n'est pas un émulateur jouant des ROM hacks, c'est un moteur écrit à partir de zéro.

## À propos du projet

### Comment puis-je contribuer au projet ?

Merci ! Rendez-vous sur la [page de contribution](/fr/development/how-to-contribute) pour en savoir plus.

### Y a t-il une entité légale derrière le projet ?

Oui, l'entité légale qui supporte le projet est l'association française Loi 1901 [Solarus Labs](/fr/about/nonprofit-organization). C'est un organisme à but non-lucratif. Les dons faits au projet iront totalement à l'association, et seront entièrement utilisés pour le projet et uniquement pour cela.

### Comment puis-je contacter l'équipe derrière Solarus ?

Si vous avez une requête ou question qui n'est pas ici, et voulez une réponse rapide :

* [Les forums](http://forum.solarus-games.org/)
* [Le tchat Discord](https://discord.gg/PtwrEgZ)
* [La page de contact](/fr/about/contact)
* [L'équipe derrière le projet](/fr/about/contributors) si vous souhaitez contacter quelqu'un en particulier.

[/column]

[column width ="4"]
[summary level-min="2" level-max="3"]
[/column]

[/row]

[/container]
