[container layout="small"]

# {title}

## Forme légale de Solarus Labs

![Solarus Labs logo](images/solarus-labs-logo.png)

[Solarus Labs](/fr/about/nonprofit-organization) est une **association française à but non lucratif**, régie par la loi du 1er juillet 1901 (selon la loi française). Il s'agit donc d'un organisme bénévole sans aucune répartition des bénéfices, ni pour ses dirigeants, ni pour ses membres. Solarus Labs, comme toute association, est une personne morale ayant sa propre capacité juridique, et sa responsabilité, distincte des personnes la dirigeant.

Le bénévolat est la règle concernant l'association : tous les dons et recettes sont conservés et utilisés par la personne morale, c'est-à-dire l'association, sans aucune répartition aux dirigeants ou aux membres, malgré leur travail et leur investissement.

## Droit applicable

![French Government logo](images/french_gouv_logo.png)

L'association Solarus Labs est une association de **droit français**. Elle est domiciliée en France et exerce l'essentiel de la gestion de son activité dans ce pays. La loi applicable la concernant (ainsi que ses logiciels et services) est donc dans tous les cas la loi française, comme prévu par les articles 14 et 15 du Code civil.

## Licences et droit d'auteur

### Logiciels

![GNU GPL v3 logo](images/gpl_v3_logo.png)

Les logiciels proposés par Solarus Labs sont tous des **logiciels libres et open source**, sous la licence [GNU GPL v3](https://www.gnu.org/licenses/quick-guide-gplv3.html). L'ensemble des codes source des logiciels est accessible sur [Gitlab](https://gitlab.com/solarus-games).

Cette licence autorise toutes les utilisations des logiciels et leur redistribution sous la même forme ou après modification, à condition que ces logiciels modifiés respectent les conditions de la licence d'origine, notamment la publication du code source et l'autorisation de redistribution.

### Ressources

![CC-BY-SA](images/cc_by_sa.png)

Les ressources produites par Solarus Labs sont toutes **libres**, sous licence [Creative Commons Attribution-ShareAlike 4.0 International 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Les ressources incluent tout type de contenu produits dans le cadre du projet Solarus: images, sons, textes, contenu de ce site Internet, etc.

Ce qui ne signifie pas que ces ressources sont dans le domaine public pour autant. Cette licence autorise l'utilisation, le partage et la modification, à condition que les ressouces modifiées respectent les conditions de la licence d'origine, notamment le crédit de l'auteur de l'œuvre originelle.

### Propriété intellectuelle

Subsidiairement aux conditions de la license, le droit commun français de la propriété intellectuelle s'applique, notamment le droit d'auteur. Tout changement de licence ou redistribution sous d'autres conditions est interdite. De même, la provenance des logiciels, même partiellement réutilisés, doit toujours être visible par les utilisateurs.

## Brevets

Selon l'article L 611-10 du Code de la propriété intellectuelle, les brevets logiciels ne sont pas admis en droit français. Au niveau communautaire, l'article 52 de la Convention sur le brevet européen a également exclu la protection des programmes d'ordinateur.

Par conséquent, les logiciels fournis par Solarus Labs ne sont pas redevables à des licences sur n'importe quel brevet logiciel, quelle qu'en soit sa provenance.

## Responsabilité d'utilisation des logiciels

L'association Solarus Labs décline toute responsabilité quand à une utilisation illégale de ses logiciels.

[/container]
