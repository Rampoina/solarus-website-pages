[container layout="small"]

# {title}

## Pourquoi faire un don

Tous les logiciels créés dans le cadre du projet de moteur de jeu Solarus sont [libres et open-source](https://gitlab.com/solarus-games), et le seront pour toujours. Cela inclut le moteur lui-même, mais également les outils pour les joueurs et développeurs, tous les jeux faits par Solarus Team, le moteur de site web, etc. Nous passons beaucoup de temps sur ces projets, et nous n'avons pas l'intention d'arrêter !

Voici à quoi peuvent par exemple servir les dons :

* Payer des développeurs ou des artistes afin qu'ils travaillent sur le projet
* Payer les coûts d'hébergement du site
* Acheter le matériel nécessaire pour déveloper Solarus (machines, logiciels)
* Couvrir les frais de transport pour les réunions, conférences ou autres évènements
* Créer du merchandising Solarus (autocollants, t-shirts, posters, etc.)

L'argent sera reversé à [Solarus Labs](/fr/about/nonprofit-organization), l'association qui soutient Solarus, et sera totalement réinvesti dans le projet.

![Logo de Solarus Labs](images/solarus-labs-logo-small.png)

Vous pouvez faire un don du montant que vous voulez via les moyens suivants. [Contactez-nous](/fr/about/contact) si vous désirez faire un don avec un moyen autre que les suivants. Votre support fait extrêmement plaisir !

## Comment faire un don

[paypal title="Paypal" email="solarus-labs@solarus-games.org" name="Don à Solarus Labs" label="Faire un don" icon="paypal" icon-category="fab" subject="Solarus"]

Utilisez Paypal pour faire un don. Vous n'avez pas besoin d'avoir un compte Paypal pour le faire.

[/paypal]

[donation title="Liberapay" label="Faire un don" icon="donate" link="https://liberapay.com/solarus-labs/donate"]

Utilisez Liberapay pour faire un don à Solarus Labs régulièrement. Liberapay est une association française qui gère une plateforme de dons récurrents.

[/donation]

[bitcoin title="Bitcoin" image="/data/assets/images/bitcoin_qrcode.png" icon="bitcoin" icon-category="fab" label="Faire un don" link="bitcoin:1MdMLRPFZf3tq3CqWMjoLqfY1enjhzJqVg"]

Utilisez l'adresse ou le QR-Code suivants:
[align type="center"]

#### [`1DyA88zEr1P2phrMWm76QNxc82RRC3nR95`](bitcoin:1MdMLRPFZf3tq3CqWMjoLqfY1enjhzJqVg)

[![Bitcoin QR Code](../../../../assets/images/bitcoin_qrcode.png)](bitcoin:1DyA88zEr1P2phrMWm76QNxc82RRC3nR95)

[/align]

[/bitcoin]

[/container]
