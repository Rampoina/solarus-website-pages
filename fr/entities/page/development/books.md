[container]

# {title}

[row]

[column width="3"]
[/column]

[column width="9"]
<!-- Searchbox -->
[entity-search text-placeholder="Rechercher"]
[/column]

[/row]

[space]

[row]

<!--Filters-->
[column width="3"]
[box-highlight]
[entity-filter entity-type="book" filter="categories" title="Catégories"]
[/box-highlight]
[/column]

<!--Books -->
[column width="9"]
[book-listing orderby="index" section="tutorial"]
[/column]

[/row]

[/container]
