<!--Header-->
[container layout="full"]
[cover is-parallax="true"]
[row]
[column width="7"]
[jumbotron title="Un moteur de jeu Action-RPG 2D léger, libre et open-source"]
[hr width="60"]

* Un moteur de jeu 2D écrit en C++, et qui exécute des jeux en Lua.
* Spécifiquement créé pour les classiques de l'Action-RPG de l'ère 16-bits.
* Disponible sur de nombreux systèmes.
* Complètement libre et open-source, sous licence GPL v3.

[space]

[button type="primary" outline="true" label="Découvrir" url="/fr/solarus/overview"]
[space orientation="vertical" thickness="20"]
[button type="primary" label="Télécharger" url="/fr/solarus/download"]
[space]

[icon icon="windows" category="fab" size="2x"]
[icon icon="apple" category="fab" size="2x"]
[icon icon="linux" category="fab" size="2x"]
[icon icon="freebsd" category="fab" size="2x"]
[icon icon="android" category="fab" size="2x"]
[icon icon="raspberry-pi" category="fab" size="2x"]

[space thickness="10"]

[small]Version stable 1.6.2 • Mise à jour le 16 août 2019[/small]

[space thickness="40"]

[/jumbotron]
[/column]

[column width="5"]
[align type="left"]
![Illustration](images/illustration.svg)
[/align]
[/column]
[/row]

[/cover]
[/container]

<!--Games-->
[highlight]
[space]
[container]

# Jeux

[game-featured id="game-the-legend-of-zelda-xd2-mercuris-chess" title="À l'affiche" button-label="More info"]

## Disponibles

[row]
[column]
[thumbnail id="game-defi-de-zeldo-ch-2"]
[/column]
[column]
[thumbnail id="game-the-legend-of-zelda-mystery-of-solarus-dx"]
[/column]
[column]
[thumbnail id="game-tunics"]
[/column]
[column]
[thumbnail id="game-the-legend-of-zelda-return-of-the-hylian-se"]
[/column]
[/row]
[space thickness="15"]

## À venir

[row]
[column]
[thumbnail id="game-vegan-on-a-desert-island]
[/column]
[column]
[thumbnail id="game-oceans-heart"]
[/column]
[column]
[thumbnail id="game-the-legend-of-zelda-a-link-to-the-dream]
[/column]
[column]
[thumbnail id="game-the-legend-of-zelda-onilink-begins-se"]
[/column]
[/row]
[space thickness="60"]

[container_1 layout="small"]
[row]
[column width="3"]
[/column]
[column width="6"]
[button-highlight type="primary" icon="bag" url="/fr/games" label="Bibliothèque de quêtes Solarus" subtitle="Davantage de jeux dans la"]
[/column]
[/row]
[/container_1]

[space thickness="20"]

[/container]
[/highlight]

<!--Getting Started-->
[container]
[space thickness="0"]

# Prise en main

Si vous désirez créer un jeu avec Solarus, vous trouvez ici de quoi vous initier.

[space thickness="20"]

[container_1 layout="small"]
[row]
[column]

[button-highlight type="primary" icon="owl" url="/fr/development/tutorials" label="Tutoriels"]

[/column]
[column]

[button-highlight type="primary" icon="book" url="http://www.solarus-games.org/doc/latest" label="Documentation"]

[/column]
[column]

[button-highlight type="primary" icon="talk" url="http://forum.solarus-games.org/" label="Forums"]

[/column]
[/row]
[/container_1]
[/container]

[space]

[container]

# Dernières actualités

[article-listing read-more-label="Lire la suite..." column-width="4" count="3"]

[container_1 layout="small"]
[row]
[column width="3"]
[/column]
[column width="6"]
[button-highlight type="primary" icon="calendar" url="/fr/news" label="Blog de développement Solarus" subtitle="Davantage d'articles sur"]
[/column]
[/row]
[/container_1]

[/container]
