[container]
[row]

[column width="8"]
[model id="article" context-id="{delegate_entity_id}"]
[/column]

[column width="4"]
[box-highlight]
[article-recents title="Derniers articles" count="10"]
[/box-highlight]
[/column]

[/row]
[/container]