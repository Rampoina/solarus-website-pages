<p>FAQ = Frequently Asked Questions, c'est-à-dire une page où vous trouverez les réponses aux questions que je reçois tous les jours par e-mail. Pensez donc à lire la FAQ avant de me contacter, car il y a de fortes chances pour que votre question y soit déjà.</p>

<p><a href="http://www.zelda-solarus.com/interact.php3?page=faq">Lire la FAQ</a></p>