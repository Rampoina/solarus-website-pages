<p>Salut à tous !</p>

<p>Le développement de la démo a beaucoup avancé ces derniers temps, même si c'est encore loin d'être fini. Nous avons déjà passé des dizaines d'heures dessus et c'est ce qui explique le manque de mises à jour ces derniers temps. Ca représente beaucoup de travail, mais le résultat sera de qualité.</p>

<p>Je suis actuellement en train de m'occuper de dessiner les salles du donjon avec TGF. Ca avance bien. Le plus long et le plus difficile a été de mettre au point les librairies de graphismes et les palettes.</p>

<p>La prochaine étape sera la programmation du jeu par Netgamer. Nous ferons des nouveaux screenshots dès que nous pourrons, c'est promis !</p>

<p>Le titre final du jeu restera secret le plus longtemps possible. Nous avons donc prévu un titre provisoire pour la démo. Ce titre sera dévoilé très bientôt...</p>

<p>Comme nous l'avions annoncé sur le chat la semaine dernière, la démo débarquera (sauf indicent grave) sur vos écrans avant Noël. Si la programmation se déroule sans problèmes, la démo pourrait même sortir bien avant cette date ;)</p>