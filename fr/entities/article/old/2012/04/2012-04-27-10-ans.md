[b]10 ans déjà ![/b]

Le 26 avril 2002, sortait notre première création, Zelda Mystery of Solarus.

[img]http://www.zelda-solarus.com/images/zs/solarus-titre.jpg[/img]

Réalisé avec RPG Maker 2000, c'était l'un des premiers Zelda amateurs complets. Son développement a duré un an et 4 mois, une belle prouesse. Il est vrai que le moteur jeu était basique... et que j'avais peut-être plus de temps libre qu'aujourd'hui :)

En 10 ans, le jeu a été téléchargé 640 000 fois. S'il est aujourd'hui dépassé par son successeur [url=http://www.zelda-solarus.com/jeu-zsdx]Zelda Mystery of Solarus DX[/url], il aura marqué marqué l'histoire des Zelda amateurs.
Merci à tous pour votre fidélité et votre passion. ^_^

Bon anniversaire ZS !

[img]http://www.zelda-solarus.com/images/uploads/animpubzs.gif[/img]
