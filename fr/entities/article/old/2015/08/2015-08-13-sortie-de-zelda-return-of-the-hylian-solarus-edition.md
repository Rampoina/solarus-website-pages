<a href="../data/fr/entities/article/old/2015/08/images/zelda_roth_title_vector.png"><img class=" size-medium wp-image-37937 aligncenter" src="/images/zelda_roth_title_vector-300x265.png" alt="zelda_roth_title_vector" width="300" height="265" /></a>

Parfois, les poissons d'avril n'en sont pas, et celui de cette année va confirmer la tendance sur Zelda Solarus. Souvenez-vous qu'on avait annoncé la sortie d'un remake de <strong>Zelda Return of the Hylian</strong>. Sachez que ce n'était pas un mensonge : le voici bel et bien !

Certes, le jeu ne sort pas sur Wii U, et il ne s'appelle pas "Return of the Return" (c'était la fausse information du poisson d'avril), mais il était bel et bien en préparation depuis plus d'un an. Aujourd'hui nous vous présentons cette nouvelle version du jeu original de Vincent Jouillat sorti en 2006.

Christopho, aidé de Mymy et Vincent Jouillat en personne, ont totalement reproduit l'original, cette fois-ci en utilisant le moteur Solarus pour améliorer l'expérience de jeu.

Si vous n'y avez jamais joué, vous verrez que tout en restant dans l'univers graphique de A Link to the Past, c'est un tout autre style que les créations de Christopho : des salles bien rectangulaires, pas de labyrinthes farfelus ou d'énigmes à s'arracher les cheveux ! Zelda Return of the Hylian est un jeu plus accessible et relativement court, mais qui n'est que le début d'une formidable quadrilogie.

Vous pouvez télécharger le jeu sur sa <a href="http://www.zelda-solarus.com/zs/article/zroth-telechargements/">page dédiée</a>. Comme on s'attend à ce que la première version ne soit pas parfaite, n'hésitez pas à nous faire part des bugs éventuels. Outre les corrections de bugs, nous prévoyons aussi dans les jours qui suivent d'intégrer des améliorations que nous n'avons pas eu le temps de terminer pour le jour J, comme les touches configurables y compris avec une manette, ou encore la version anglaise.