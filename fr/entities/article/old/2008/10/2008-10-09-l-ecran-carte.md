Le projet Zelda Solarus DX avance chaque jour un peu plus :). Depuis la dernière news, j'ai fait l'écran Carte. Sans plus attendre, voici une capture d'écran :

[center][img]http://www.zelda-solarus.com/images/zsdx/world_map_submenu.png[/img][/center]

Vous reconnaissez ici la carte du monde extérieur. Pour l'instant, c'est simplement une version réduite de celle de Mystery of Solarus puisque les maps extérieures de la version DX ne sont pas encore commencées. La position de Link s'affiche sur la carte à l'endroit où il est, y compris s'il est dans une maison (comme ici). Mais la carte du monde est un trésor que vous devrez trouver dans le jeu, donc au début, l'écran Carte n'affiche que des nuages, un peu comme dans Ocarina of Time ;).

Lorsque vous êtes dans un donjon, la carte du monde laisse place à celle du donjon. Comme dans tous les Zelda, si vous avez la Carte, le plan s'affiche, ainsi que la position de Link, des coffres et du boss si vous possédez la Boussole. Vous pouvez sélectionner les différents étages et aussi voir les objets du donjon que vous avez déjà récoltés : Carte, Boussole, Grande Clé, Clé du Boss et Petites Clés. Tout ceci vient d'être réalisé mais je ne peux pas encore vous le montrer puisque les donjons ne sont pas encore commencés :P.

D'ici la fin de la semaine, j'espère finir le menu de pause : il reste à faire l'écran de statut quête et celui des options. Après, je passerai à complètement autre chose : le système de collisions au pixel près, première étape nécessaire pour le futur système de combats  ^_^.