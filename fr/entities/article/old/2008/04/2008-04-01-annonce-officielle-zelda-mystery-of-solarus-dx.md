Bonjour à tous,

[center][IMG]http://www.zelda-solarus.com/images/actu/zsdx/logo_mini.jpg[/IMG][/center]

La rumeur qui court depuis cet après-midi sur le forum est officiellement confirmée : une version &quot;Deluxe&quot; de notre création Zelda Mystery of Solarus est en cours de développement par un fan de Zelda ! Il s'agit d'un remake, c'est-à-dire le même jeu réédité et largement amélioré. L'outil de développement qui a été choisi est le langage C++. ^_^

[b]Pourquoi le C++ ?[/b]

Comme vous le savez, l'actuel Mystery of Solarus avait été développé avec RPG Maker. L'avantage de ce logiciel de création de jeux, c'est qu'il permet de faire des jeux assez rapidement. L'inconvénient, c'est qu'il est très limité et qu'il est prévu à la base pour faire des RPG. Le système de combats n'est pas terrible, il faut mettre pause pour utiliser des objets, les boss sont statiques, etc...
Pour toutes ces raisons, Mercuris' Chest utilisait un logiciel plus performant (Multimedia Fusion) mais au final, de nombreux problèmes ont été rencontrés, ce qui a eu pour conséquence l'annulation du projet il y a un an.
Depuis plusieurs mois déjà, une réédition de Mystery of Solarus en C++ était donc en cours de développement dans le plus grand secret. Les graphismes seront bien sûr les mêmes, avec quelques ajouts d'éléments inédits. Les maps seront plus détaillées. Les musiques seront en partie celles de A Link to the Past, et en partie des musiques inédites ou retouchées. Le gameplay sera nettement supérieur à Mystery of Solarus, et il devrait être au moins équivalent à Mercuris' Chest. L'écran de jeu est d'ailleurs repris de Mercuris' Chest.

[b]Les images[/b]

Sans plus attendre, voici les toutes premières images officielles du projet (cliquez pour agrandir) !

[center]
[IMG]http://www.zelda-solarus.com/images/actu/zsdx/title_screen.png[/IMG] [IMG]http://www.zelda-solarus.com/images/actu/zsdx/selection_menu.png[/IMG]
[IMG]http://www.zelda-solarus.com/images/actu/zsdx/rupee_house.png[/IMG] [IMG]http://www.zelda-solarus.com/images/actu/zsdx/link_house_1.png[/IMG]
[/center]

On peut voir ici les premières maps, celles des maisons du début du jeu : les connaisseurs auront sans doute reconnu la maison de Link et la maison des Rubis. Il y a aussi l'écran titre, qui a été totalement refait pour l'occasion, et enfin le traditionnel menu de sélection des sauvegardes :)

Il est trop tôt pour parler d'une date de sortie, même pour une démo, car le développement demande beaucoup de travail. Pour information, ce qui a été fait jusqu'à présent représente déjà plus de 400 heures de travail et 10000 lignes de code.
Nous vous donnerons d'autres d'informations sur l'avancement sur ce projet prometteur dans les prochaines heures et dans les jours à venir ;)