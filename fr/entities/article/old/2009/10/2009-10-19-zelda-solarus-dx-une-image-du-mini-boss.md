Les derniers éléments de la démo de notre projet [url=http://www.zelda-solarus.com/jeu-zsdx]Zelda Mystery of Solarus DX[/url] sont désormais réalisés. Il s'agit de l'intro, imaginée par Neovyse, et du mini-boss du donjon, une création inédite de @PyroNet. Voici une capture d'écran de ce dernier, même si je vous accorde qu'il est difficile de s'imaginer comment cela peut rendre dans le jeu à partir d'une simple image :
[center][img]http://www.zelda-solarus.com/images/zsdx/khorneth.png[/img][/center]
Ce mini-boss répond au nom de Khorneth et étant donné que c'est le premier mini-boss du jeu, il est relativement facile à vaincre. Cela dit, les testeurs examineront si sa difficulté mérite d'être réajustée :). Le boss du premier donjon, quant à lui, vous donnera plus de fil à retordre. Prévoyez vos fées ! En tout cas, nous vous avions promis des boss intéressants et inédits, donc c'est bien parti :)

[list]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-images]Galerie d'images de Mystery of Solarus DX[/url][/li]
[/list]

A 2 mois de la sortie officielle de la démo, la dernière phase peut donc commencer : les tests intensifs ainsi que les traductions dans d'autres langues.
J-60 !