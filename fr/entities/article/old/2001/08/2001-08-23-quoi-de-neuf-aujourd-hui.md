<p>J'ai beaucoup avancé en ce vendredi de vacances ensoleillées...</p>

<p>J'ai enfin réglé définitivement les bugs liés au G... oups, j'ai failli vous révéler quel était le Trésor du troisième donjon ! Quoi qu'il en soit, j'ai passé au moins deux heures aujourd'hui à reparamétrer tous les événements concernés par cet objet.</p>

<p>Tout ça pour vous dire que les bugs du Trésor du donjon 3 sont enfin corrigés et que le développement a repris une vitesse normale. En effet tout ce qui est entre les donjons 3 et 4 est maintenant quasiment terminé. Je vais donc très bientôt m'attaquer au quatrième donjon !</p>