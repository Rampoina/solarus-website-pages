<p>Ouvert depuis le 4 Juillet, le mini-site dédié à Zelda : Mystery of Solarus connaît un grand succès et à partir d'aujourd'hui il est devient un site à part entière !</p>

<p>L'adresse courte reste la même : www.zelda-solarus.fr.st, mais le site est maintenant indépendant de <a href="http://www.consolespower.fr.st" target="_blank">Consoles&nbsp;Power</a>.</p>

<p>Cette petite surprise était en préparation depuis quelques jours, ce qui explique le manque de mises à jour. Maintenant que le site est opérationnel les mises à jour régulières vont revenir très vite.</p>

<p>Plusieurs changements sont déjà visibles sur le site : l'image de Link a changé, ainsi que les rubriques. La rubriques news a disparu, puisque tous les news sont déjà visibles sur la page d'accueil. De plus j'ai remplacé le lien vers Consoles Power par un petit logo en bas de chaque page.</p>

<p>D'autres nouveautés feront leur apparition prochainement, en particulier au niveau du design. Une version 2.0 du site n'est pas impossible :)</p>

<p>N'hésitez pas à échangez vos impressions sur le <a href="http://www.zelda-solarus.com/forum.php3">Forum</a> qui ne demande qu'à être rempli ! Si vous êtes bloqué(e) dans la démo du jeu alors c'est la meilleure solution.</p>