<p>Vous ne rêvez pas, la démo de Zelda : Advanced Project est sortie ! Nous avons en effet préféré ne pas annoncer de date pour éviter d'avoir à nous précipiter pour la finir.</p>

<p>La démo fait 3.3 Mo. On estime la durée de vie à quelques heures. N'oubliez pas de lire le mode d'emploi avant de jouer. On attend vos réactions dans les commentaires de cette news ou sur le forum !</p>

<p>Amusez-vous bien !</p>

<p><a href="http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=notice">Mode d'emploi de la démo</a></p>

<p><a href="http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=download">Télécharger la démo</a></p>