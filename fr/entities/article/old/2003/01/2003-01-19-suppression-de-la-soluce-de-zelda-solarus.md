<p>Que ce soit par e-mail, sur le forum ou sur msn messenger, le constat est le même : on reçoit sans arrêt des messages de personnes bloquées au niveau 5 de Zelda Solarus. Or, la soluce s'arrête justement au début du niveau 5.</p>

<p>Comme expliqué dans la FAQ, nous ne sommes pas là pour gâcher la durée de vie du jeu. Il est donc hors de question de finir la soluce maintenant. Nous nous sommes donnés assez de mal pour créer le jeu, alors ça serait dommage que vous le finissiez trop facilement à l'aide d'une soluce.</p>

<p>Depuis que nous avons mis en ligne la première partie de la soluce, les joueurs la consultent au moindre problème. Résultat, quand il n'y a plus de soluce, ils ne savent plus quoi faire. La difficulté du jeu est progressive. Si vous utilisez la soluce au
début du jeu au lieu de chercher par vous-mêmes, il est évident que quand ça devient plus dur et qu'en plus la soluce n'est plus là, ça coince. Et si nous mettions en ligne la soluce du niveau 5, tout le monde serait bloqué au niveau 6 qui est encore plus dur que le 5, et ainsi de suite !</p>

<p>C'est pour cette raison que nous venons d'enlever la soluce du site. Comme ça les joueurs seront bien
obligés de se creuser la tête au début du jeu, et ils arriveront ainsi à résoudre des énigmes de plus en plus difficiles et à suivre la difficulté progressive du jeu !</p>

<p>Vous nous réclamez la suite de la soluce depuis des mois, et bien il faudra encore attendre :-) Bien sûr nous la remettrons en ligne un jour, mais pas avant la sortie de la démo de ZAP en tout cas ! En attendant, le moyen le plus efficace pour vous débloquer est de laisser un message sur le <a href="http://www.zelda-solarus.com/forums">forum</a>. Mais avant d'en arriver là, essayez de trouver vous-même la solution aux énigmes, c'est quand même beaucoup satisfaisant !</p>
