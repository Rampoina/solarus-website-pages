<p>Ca faisait un certain temps que nous ne vous avions pas donné de screenshots de notre prochaine création : Zelda Advanced Project (titre provisoire), alors on se rattrape un peu aujourd'hui ! Voici un screenshot du menu de sélection des sauvegardes de la démo, que nous venons juste de finir :</p>

<p align="center"><img src="/images/fileselect.gif" width=320 height=240></p>

<p>Comme vous pouvez le constater c'est autre chose qu'avec RPG Maker :-)</p>

<p><a href="http://www.zelda-solarus.com/jeux.php?jeu=zf&zone=scr">Voir les autres screenshots</a></p>