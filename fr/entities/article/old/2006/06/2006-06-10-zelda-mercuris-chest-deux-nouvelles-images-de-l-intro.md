Cela fait depuis quelques semaines que Newlink et moi-même avons repris le travail en ce qui concerne l'intro du jeu. Newlink dessine les personnages et les diverses animations, tandis que je m'occupe de créer les maps et de programmer le déroulement de l'intro. Je n'avais pas eu beaucoup le temps de m'en occuper depuis le mois de mai, mais je m'y suis remis aujourd'hui et jusque tard dans la nuit, comme en témoigne l'heure de cette mise à jour. Nous avons donc deux nouvelles captures d'écran à vous proposer :

[center][img]http://www.zelda-solarus.com/images/zf/zmc/intro2.png[/img]

[img]http://www.zelda-solarus.com/images/zf/zmc/intro3.png[/img][/center]

Comme vous le voyez, au début de l'intro, Link dort paisiblement, comme dans presque tous les Zelda (y compris Mystery of Solarus :P).

Contrairement à la première image que nous avions dévoilée il y a quelques mois, ces deux captures d'écran sont réellement tirées du jeu. Pour la [url=http://www.zelda-solarus.com/images/zf/zmc/intro.png]première image[/url] que nous avions diffusée, nous avions triché : à l'époque, la map avait été faite seulement avec un logiciel de dessin et les deux personnages avaient été ajoutés dessus manuellement. Mais aujourd'hui, les maps d'où proviennent ces images existent réellement et on peut d'ores et déjà lancer le début de l'intro.

Ne comptez pas sur moi pour vous en dire plus sur ce qui se passe dans l'intro ou dans le scénario en général ^_^
Cela dit, les plus malins sauront sans doute déduire des informations et faire des hypothèses à partir de ces images ;)

Vous aurez des nouvelles infos et des nouvelles images au fur et à mesure de l'avancement du projet. Bien que je travaille cet été, je vais tout faire pour consacrer un maximum de temps à Zelda Mercuris' Chest !

[url=http://www.zelda-solarus.com/jeux.php?jeu=zmc&amp;zone=scr]Galerie d'images de Zelda : Mercuris' Chest[/url]
