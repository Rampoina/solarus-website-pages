<p>De nombreux joueurs anglophones nous réclament une version anglaise de Zelda : Mystery of Solarus. Malheureusement, le jeu n'avait pas été prévu au départ pour être traduit un jour et ça demanderait maintenant trop de travail.</p>

<p>En revanche, Zelda : Mercuris' Chest va être traduit. Et c'est notre cher Géomaster qui se chargera de cette traduction. Je suis donc heureux de vous annoncer que la démo 2.0 en version anglaise sortira le 10 août, parlez-en autour de vous !</p>

<p>Géomaster n'est pas le premier venu en matière de traduction puisqu'il a traduit de l'anglais vers le français les mangas de Ocarina of Time, Oracle of Ages et Oracle of Seasons sur Zelda Solarus. Il a déjà également traduit en anglais la démo d'un Zelda amateur : il s'agit de <a href="http://zaots.free.fr" target="_blank">Zelda : Attack of the Shadow</a>, le jeu de Doc (un peu de pub ne fait pas de mal !).</p>

<p>Par ailleurs, le <a href="http://www.zelda-solarus.com/jeux.php?jeu=zmc&zone=notice">mode d'emploi</a> de la démo vient d'être mis à jour pour prendre en compte les nouveautés de la version 2.0. Ce mode d'emploi sera également disponible en version anglaise pour le 10 août.</p>