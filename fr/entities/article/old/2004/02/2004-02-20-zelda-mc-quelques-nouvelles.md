<p>Zelda MC, ce sont les initiales du titre définitif de notre prochaine création, anciennement connue sous le nom de Zelda : Advanced Project. En ce moment le projet avance bien, tant au niveau de la programmation qu'au niveau du scénario.</p>

<p>Concernant la programmation, je m'occupe toujours de programmer le moteur de jeu mais ça se passe bien, lentement mais sûrement. On est encore loin de programmer les premières maps mais ça finira bien par arriver un jour ;)</p>

<p>Niko va continuer à travailler sur le scénario. Le jeu se déroulera en deux parties. La première partie est écrite dans l'ensemble, il reste à peaufiner quelques points de détails pour que ça soit définitif. Ensuite on pourra attaquer la deuxième partie du scénario...</p>

<p>La bonne nouvelle du jour, c'est que nous avons enfin trouvé un titre de qualité pour le jeu. Et oui nous l'avons encore changé, car le précédent (qui était secret) était trop concret alors que celui que nous venons de trouver identifiera bien le jeu. C'est donc la troisième fois que nous changeons d'idée pour le titre, sans compter "Zelda : Advanced Project" qui n'est qu'un nom de code ! Mais cette fois c'est seulement en partie, car les initiales Zelda : MC qui ont été dévoilées il y a quelque temps déjà restent valables.</p>

<p>Quoi qu'il en soit nous gardons le titre secret pour l'instant, et ne soyez pas trop impatients pour ce qui est de la date de sortie car le travail est encore long.</p>