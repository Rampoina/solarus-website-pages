Le semaine dernière, j'ai travaillé d'arrache-pied à l'élaboration du <strong>Temple du Masque</strong>. Pour ceux qui ne connaissent pas ce lieu mythique, il s'agit du sixième donjon de Link's Awakening. A ce stade, je peux vous dire qu'il est terminé à 80% et promet d'être épique !

Voici deux images qui représentent des salles que vous reconnaitrez peut être.

<a href="../data/fr/entities/article/old/2018/01/images/masque1.png"><img class="alignnone size-medium wp-image-38254" src="/images/masque1-300x240.png" alt="" width="300" height="240" /></a> <a href="../data/fr/entities/article/old/2018/01/images/masque2.png"><img class="alignnone size-medium wp-image-38255" src="/images/masque2-300x240.png" alt="" width="300" height="240" /></a>

N'hésitez pas à commenter et à dire ce que vous attendez le plus dans ce donjon ou même dans le projet !
En attendant, je vous dis à la semaine prochaine, pour de nouvelles informations croustillantes.