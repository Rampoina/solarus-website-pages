Ces derniers jours, nous avons beaucoup travaillé sur le projet de <strong>remake de Link's Awakening</strong> notamment au niveau des cinématiques. C'est vraiment très intense car nous utilisons les dernières avancées du moteur. Cela nous permet vraiment d'avoir vraiment un jeu de très grande qualité.

De plus, Neovyse a retravaillé le logo du jeu qui est absolument magnifique. Merci à lui !

<a href="../data/fr/entities/article/old/2018/09/images/logo-01.png"><img class="alignnone size-medium wp-image-38355" src="/images/logo-01-300x203.png" alt="" width="300" height="203" /></a>

&nbsp;

N'hésitez pas à donner votre avis !

&nbsp;

&nbsp;