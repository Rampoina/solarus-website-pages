Bonjour à toutes et à tous,

Il y a un an jour pour jour sortait notre dernière uvre en date, Zelda XD2: Mercuris Chess.

Pour fêter ça, Neovyse a réalisé ce trailer pour le moins épique !

<iframe width="560" height="315" src="https://www.youtube.com/embed/M2tAk5hRMHk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Et surtout, grande nouvelle, le jeu est désormais disponible en anglais. La traduction anglaise était attendue depuis longtemps, étant donné le succès de son prédécesseur Zelda Mystery of Solarus XD qui lui est disponible en anglais et dans d'autres langues depuis plusieurs années déjà. Alors voyant la date du 1er avril approcher, la Solarus Team s'est motivée pour tenter le défi. La tâche était colossale puisque le fichier de dialogues ne contient pas moins de 7571 lignes ! C'est plus que dans Zelda Mystery of Solarus DX tout entier !

Merci à Diarandor, Neovyse, Renkineko et Morwenn pour leur aide concernant cette traduction !

Pour l'occasion, nous en avons profité pour améliorer d'autres aspects du jeu en général, notamment pour donner plus d'indications sur l'objet qu'il est fortement préférable d'avoir avant d'attaquer le premier donjon... Nous avons regardé beaucoup de let's play du jeu sur YouTube ou Twitch, et cela nous a permis d'identifier ce qui ne se passait pas exactement comme prévu ! Je ne vous en dis pas plus mais si vous n'avez encore jamais joué, c'est le moment de vous y mettre ! Vous verrez que le jeu est plein de surprises, et qu'il est encore plus riche que Zelda Mystery of Solarus XD au niveau des références diverses et des situations inattendues.
<ul>
 	<li><a href="http://www.zelda-solarus.com/zs/article/zxd2-telechargements/">Télécharger Zelda XD2 1.0.5</a></li>
</ul>