Suite aux régressions apportées par la récente version 1.6.1, voici une autre mise à jour de maintenance. Solarus passe ainsi en version **1.6.2**, et les bugs des téléporteurs sont désormais corrigés.

## Changelog

### Changements pour Solarus 1.6.2

![Solarus logo](2019-08-10-solarus-1-6-1-bugfix-release/solarus_logo.png)

#### Changements du moteur

* Correction des téléporteurs défilants ayant une taille carrée (#1412).
* Correction du héros affiché au-dessus des murs des escaliers.

### Changements pour Solarus Quest Editor 1.6.2

![Solarus Quest Editor logo](2019-08-10-solarus-1-6-1-bugfix-release/sqe_logo.png)

* Correction du crash lors de la fermeture des vues tileset (#467).
* Correction des ennemis non-fonctionnels dans la quête initiale (#466).
* Installation automatique des fichiers de traduction.
* Mise à jour de la traduction française.

### Changements pour Zelda Mystery of Solarus DX 1.12.2

![Mystery of Solarus DX logo](2019-08-10-solarus-1-6-1-bugfix-release/mos_dx_logo.png)

* Correction de téléporteurs défilants cassés dans Solarus 1.6.1.

### Changements pour Zelda Mystery of Solarus XD 1.12.2

![Mystery of Solarus XS logo](2019-08-10-solarus-1-6-1-bugfix-release/mos_xd_logo.png)

* Correction de téléporteurs défilants cassés dans Solarus 1.6.1.
