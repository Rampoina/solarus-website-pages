# {title}

[youtube id="Y0fzDSgk7SI"]

## Sommaire

- Configurer les scripts de HUD (menus)
  - Cœurs
  - Rubis
  - Objet
  - `attack_icon` et `action_icon`

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)

Vous pouvez réutiliser dans vos projets les éléments de HUD de nos jeux :

- [Zelda ROTH SE](https://gitlab.com/solarus-games/zelda-roth-se) ou [Zelda OLB SE](https://gitlab.com/solarus-games/zelda-olb-se): scripts de HUD with dans le style d'*A Link to the Past*.
- [Zelda MOS DX](https://gitlab.com/solarus-games/zsdx) ou [Zelda Mercuris' Chest](https://gitlab.com/solarus-games/zelda-mercuris-chest) : scripts de HUD dans le style d'*Ocarina of Time*.
