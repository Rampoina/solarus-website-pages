# {title}

[youtube id="UNwFFsr1V-Y"]

## Sommaire

- Utiliser les escaliers pour relier des maps
- Créer des trous (téléporteurs) pour tomber vers une autre map
  - Définit la propriété d'étage de la map

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
