# {title}

[youtube id="v0nK8GoNJNg"]

## Sommaire

- Utiliser les entités interrupteurs
  - Interrupteur sur lequel le héros peut marcher
  - Interrupteur non-traversable
- Utiliser un interrupteur dans un script de map
  - Faire apparaître un coffre (et sauvegarder cet état)
- Evènements utilisés par un interrupteur
  - L'évènement `on_activated()`
  - L'évènement `on_deactivated()`

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
