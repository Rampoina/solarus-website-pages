# {title}

[youtube id="P45FQDdUsFg"]

## Sommaire

- Mettre le jeu en pause
- Créer un dialogue de sauvegarde
- Les dialogues avec des questions
- Sauvergarder la partie
- Structure du fichier de sauvegarde
- Définir des valeurs dans le fichier de sauvegarde

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
